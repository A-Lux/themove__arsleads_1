<?php
class ControllerExtensionModuleTMSocialList extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/tm_social_list');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_desc'] = $this->language->get('heading_desc');

		$i = 0; foreach ($setting['socials'] as $social) {
			$data['socials'][$i] = array(
				'name' => $social['name'],
				'link' => $social['link'],
				'css'  => $social['css']
				);
			$i++;
		}
		return $this->load->view('extension/module/tm_social_list', $data);
	}
}