<div class="box-social">
	<h3><?php echo $heading_title; ?></h3>
	<p><?php echo $heading_desc; ?></p>
	<ul class="social-list list-unstyled">
		<?php foreach ($socials as $social) { ?>
		<li><a class="<?php echo $social['css']; ?>" href="<?php echo $social['link']; ?>" data-toggle="tooltip" title="<?php echo $social['name']; ?>"></a></li>
		<?php } ?>
	</ul>
</div>