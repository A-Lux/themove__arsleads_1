<?php if ($articles) { ?>
<div class="box blog_articles">
	<div class="box-heading">
		<h2><?php echo $heading_title; ?></h2>
	</div>
	<div class="box-content">
			<?php foreach ($articles as $article) { ?>
			<div class="blog_articles-block">
				<div class="col-sm-6">
					<?php if ($show_image == 1) { ?>
						<figure class="article-image">
							<a href="<?php echo $article['href']; ?>"><img width="<?php echo $article['image_width']; ?>" height="<?php echo $article['image_height']; ?>" src="<?php echo $article['image']; ?>" alt="<?php echo $article['article_title'] ?>"/></a>
						</figure>
					<?php } ?>
				</div>
				<div class="container description-blog ">
					<div class="col-sm-6">
						<div>
							<?php if ($show_date == 1 || $show_author == 1 || $show_comments == 1) { ?>
							<div class="article-sub-title">
								<?php  if ($show_author == 1) {?>
								<span>
									<?php echo $tx_author; ?><a href="<?php echo $article['author_href']; ?>"><?php echo $article['author_name']; ?></a>
								</span>
								<?php }?>
								<?php if ($show_date == 1) {?><span><?php echo $article['date_added']; ?></span><?php }?>
								<?php if ($article['allow_comment'] && $show_comments == 1) { ?>
								<span>
									<a href="<?php echo $article['comment_href']; ?>"><?php echo $article['total_comment']; ?></a>
								</span>
								<?php } ?>					
							</div>
							<?php }?>
							<div class="article-title">
								<h3><a href="<?php echo $article['href']; ?>"><?php echo $article['article_title'] ?></a></h3>
							</div>											
							<?php if ($show_description && $article['description']) { ?>
							<div class="article-description"><?php echo $article['description']; ?></div>
							<?php } ?>
							<?php if ($show_readmore == 1) { ?>
							<a href='<?php echo $article['href']; ?>' class="btn-primary"><?php echo $text_button_continue?></a>
							<?php } ?>
						</div>
					</div>
				</div>				
			</div>
			<?php } ?>
	</div>
</div>
<div class="clear"></div>
<?php } else { ?>
<div class="buttons">
	<div class="center"><?php echo $text_no_found; ?></div>
</div>
<?php } ?>