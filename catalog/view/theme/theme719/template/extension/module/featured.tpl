<div class="box featured">
	<div class="box-heading">
		<h3><?php echo $heading_title; ?></h3>
	</div>
	<div class="box-content">
		<div class="<?php echo $layout_type ? 'box-carousel' : 'row'; ?>">
			<?php $f = 2000; $clr = 0; foreach ($products as $product) { $f++; $clr++; ?>
			<?php if ($layout_type == 0) { ?>
			<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12" <?php echo $clr%3 == 0 ? 'data-clear=""' : ''; ?>>
				<?php } ?>
				<div class="product-thumb transition <?php if ($product['options']) echo 'options';?>">
					<?php if ($product['options']) { ?>
					<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3><?php echo $text_option; ?></h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="<?php echo $product['product_id'] ?>" class="form-control"/>
									</div>
								</div>
								<?php foreach ($product['options'] as $option) { ?>
								<?php if ($option['type'] == 'select') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<option value="<?php echo $option_value['product_option_value_id']; ?>">
												<?php echo $option_value['name']; ?>
												<?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'radio') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<div id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<?php if ($option_value['image']) { ?>
											<div class="radio image-view">
												<label>
													<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
													<img width="18" height="18" src="<?php echo $option_value['image']; ?>" data-toggle="tooltip" title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) <?php } ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
												</label>
											</div>
										<?php } else { ?>
											<div class="radio">
												<label>
													<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
													<?php echo $option_value['name']; ?>
													<?php if ($option_value['price']) { ?>
													(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
													<?php } ?>
												</label>
											</div>
										<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'checkbox') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<div id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
													<?php echo $option_value['name']; ?>
													<?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>
												</label>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'image') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<div id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<div class="radio">
												<label>
													<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
													<img width="50" height="50" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail"/> <?php echo $option_value['name']; ?>
													<?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>
												</label>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'text') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control"/>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'textarea') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control"><?php echo $option['value']; ?>
										</textarea>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'file') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<button type="button" id="button-upload<?php echo $option['product_option_id'] . $module . $f; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-default">
											<i class="fa fa-upload"></i>
											<?php echo $button_upload; ?>
										</button>
										<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>"/>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'date') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<div class="input-group date">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control"/>
											<span class="input-group-btn">
												<button class="btn btn-default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'datetime') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>"><?php echo $option['name']; ?></label>
									<div class="col-sm-12">
										<div class="input-group datetime">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control"/>
											<span class="input-group-btn">
												<button type="button" class="btn btn-default">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'time') { ?>
								<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
									<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] . $module . $f; ?>">
										<?php echo $option['name']; ?>
									</label>
									<div class="col-sm-12">
										<div class="input-group time">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id'] . $module . $f; ?>" class="form-control"/>
											<span class="input-group-btn">
												<button type="button" class="btn btn-default">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<?php } ?>
								<?php } ?>
								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
							</div>
						</div>
					</div>
					<?php } ?>
					
					<div class="image">
						<a class="lazy" style="padding-bottom: <?php echo($product['img-height'] / $product['img-width'] * 100); ?>%" href="<?php echo $product['href']; ?>">
							<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" data-src="<?php echo $product['thumb']; ?>" src="#"/>
						</a>						
					</div>
					<div class="caption">	
					
						<div class="caption_stick">
						<?php if ($product['special']) { ?>
							<?php if ($label_sale) { ?>
							<div class="sale">
								<span><?php echo $text_sale; ?></span>
							</div>
							<?php } ?>
							<?php if ($label_discount) { ?>
							<div class="discount">
								<span><?php echo $product['label_discount']; ?></span>
							</div>
							<?php } ?>
						<?php } ?>
						<?php if ($product['label_new']) { ?>
							<div class="new-pr"><span><?php echo $text_new; ?></span></div>
						<?php } ?>
						</div>
						
						<div class="caption_inside">
					
						<?php if ($product['price']) { ?>
							<div class="price">
								<?php if (!$product['special']) { ?>
								<?php echo $product['price']; ?>
								<?php } else { ?>
								<span class="price-new"><?php echo $product['special']; ?></span> 
								<span class="price-old"><?php echo $product['price']; ?></span> <?php } ?>
								<?php if ($product['tax']) { ?>
								<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
								<?php } ?>
							</div>
						<?php } ?>
						
						<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
						
						<div class="cart-button">
							<button class="btn" type="button" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="ajaxAdd($(this),<?php echo $product['product_id'] ?>);" <?php } ?>><?php echo $button_cart; ?></button>
							<a class="btn quickview" href="index.php?route=extension/module/tm_ajax_quick_view/ajaxQuickView" data-rel="details" data-product="<?php echo $product['product_id']; ?>"><?php echo $text_quick; ?></a>
						</div>
						
						<div class="rating">
							<?php if ($product['rating']) { ?>							
								<?php for ($i = 1; $i <= 5; $i++) { ?>
								<?php if ($product['rating'] < $i) { ?>
								<span class="fa-stack"><i class="fl-bigmug-line-favourites5 fa-stack-1x"></i></span>
								<?php } else { ?>
								<span class="fa-stack"><i class="fl-bigmug-line-favourites5 star fa-stack-1x"></i></span>
								<?php } ?>
								<?php } ?>							
							<?php } ?>
						</div>
						
						</div>
					</div>
				</div>
				<?php if ($layout_type == 0) { ?>
			</div>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>
