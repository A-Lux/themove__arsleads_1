<div class="box-social">
	
	<ul class="social-list list-unstyled">
		<?php foreach ($socials as $social) { ?>
		<li><a class="<?php echo $social['css']; ?>" href="<?php echo $social['link']; ?>" data-toggle="tooltip" title="<?php echo $social['name']; ?>"></a></li>
		<?php } ?>
	</ul>
</div>