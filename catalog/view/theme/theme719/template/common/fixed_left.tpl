<div id="fixed-left" class="fixed-left">
	<a href="#" id="fixed-left__toggle" class="fixed-left__toggle"><i class="fa fa-filter"></i></a>
	<div class="fixed-left__cont">
		<?php foreach ($modules as $module) { ?>
		<?php echo $module; ?>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	;(function ($) {
		$(document).ready(function(){
			$('#fixed-left__toggle').on('click', function(e){
				e.preventDefault();
				$(this).parent().toggleClass('active');
			});
		});
	})(jQuery);
</script>