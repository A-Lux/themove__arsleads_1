<?php if ($modules) { ?>
<aside id="column-left" class="col-sm-3">
	<?php foreach ($modules as $module_num => $module) {
		if (isset($show_modules) && in_array($modules_ids[$module_num], $show_modules)) {
			if ($logged) {
				echo $module;
			}
		} else {
			echo $module;
		}
	} ?>
</aside>
<?php } ?>