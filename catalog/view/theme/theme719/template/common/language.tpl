<?php if (count($languages) > 1) { ?>
<div class="box-language">
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
		<div class="btn-group">
			<span class="dropdown-toggle" data-toggle="dropdown">
				<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] == $code) { ?>
				<img width="16" height="11" src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>">
				<?php echo $language['name']; ?>
				<?php } ?>
				<?php } ?>
			</span>
			<ul class="dropdown-menu list-unstyled">
				<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] == $code) { ?>
				<li>
					<button class="language-select selected" type="button" name="<?php echo $language['code']; ?>">
						<img width="16" height="11" src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" />
						<?php echo $language['name']; ?>
					</button>
				</li>
				<?php } else { ?>
				<li>
					<button class="language-select" type="button" name="<?php echo $language['code']; ?>">
						<img width="16" height="11" src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" />
						<?php echo $language['name']; ?>
					</button>
				</li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
	</form>
</div>
<?php } ?>
