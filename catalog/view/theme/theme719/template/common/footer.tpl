<footer>	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="logo">
					<?php if ($logo) { ?>
					<a href="<?php echo $home; ?>"><img src="image/catalog/logo-footer.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive"></a>
					<?php } else { ?>
					<a href="<?php echo $home; ?>"><?php echo $name; ?></a>
					<?php } ?>
				</h1>
				<?php if ($footer_top) { ?>
					<div class="footer_modules"><?php echo $footer_top; ?></div>
				<?php } ?>
			</div>		
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<?php echo $powered; ?><!-- [[%FOOTER_LINK]] -->
		</div>
	</div>
	<!-- code by xena -->
</footer>
<div class="ajax-overlay"></div>
<div class="ajax-quickview-overlay">
	<span class="ajax-quickview-overlay__preloader"></span>
</div>
</div>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/device.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/livesearch.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/script.js" type="text/javascript"></script>
</body></html>