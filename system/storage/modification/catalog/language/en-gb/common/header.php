<?php
// Text
$_['text_home']          = 'Home';

				$_['text_compare'] = 'Compare';
				
$_['text_wishlist']      =  'Wish List' ;
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      =  'Registration' ;
$_['text_login']         =  'Sign In' ;
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
