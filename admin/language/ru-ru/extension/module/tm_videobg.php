<?php
// Heading
$_['heading_title']              = 'TemplateMonster Video Background';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Успешно: Вы изменили настройки модуля TemplateMonster Video Background!';
$_['text_edit']                  = 'Редактировать модуль TemplateMonster Video Background';
$_['text_youtube']               = 'Youtube видео';
$_['text_local']                 = 'Локальное видео';
$_['text_true']                  = 'Да';
$_['text_false']                 = 'Нет';

// Entry
$_['entry_status']               = 'Статус';
$_['entry_name']                 = 'Название модуля';
$_['entry_url']                  = 'Ссылка на Youtube видео';
$_['entry_type']                 = 'Источник видео';
$_['entry_muted']                = 'Отключить звук';
$_['entry_mobile']               = 'Мобильный';
$_['entry_start']                = 'Начало воспроизведения';
$_['entry_youtube_image']        = 'Изображение для мобильных устройств';
$_['entry_local_image']          = 'Изображение видео';
$_['entry_description']          = 'Описание';
$_['entry_youtube_image_width']  = 'Ширина изображения для мобильных устройств';
$_['entry_local_image_width']    = 'Ширина изображения видео';
$_['entry_local_image_height']   = 'Высота изображения видео';
$_['entry_youtube_image_height'] = 'Высота изображения для мобильных устройств';

// Error
$_['error_permission']           = 'У вас нет разрешения на редактирование модуля TemplateMonster Video Background!';
$_['error_name']                 = 'Название модуля должно содержать от 3х до 64х символов!';
$_['error_start']                = 'Значение начала воспроизведения должно быть в цифровом формате!';
$_['error_mobile']               = 'Не задано изображение для мобильных устройств!';
$_['error_youtube_url']          = 'Необходимо заполнить поле "ссылка на Youtube видео"!';
$_['error_youtube_image_width']  = 'Значение ширины изображения для мобильных устройств должно быть в цифровом формате';
$_['error_youtube_image_height'] = 'Значение высоты изображения для мобильных устройств должно быть в цифровом формате';
$_['error_local_image']          = 'Необходимо заполнить поле "изображение видео"';