<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-social-list" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">
					<i class="fa fa-save"></i>
				</button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
					<i class="fa fa-reply"></i>
				</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li>
					<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-social-list" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
						<div class="col-sm-10">
							<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control"/>
							<?php if ($error_name) { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="status" id="input-status" class="form-control">
								<?php if ($status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<fieldset id="socials">
						<legend><?php echo $entry_socials; ?></legend>
						<?php if ($socials) { ?>
						<?php $i=0; foreach ($socials as $social) { ?>
						<div class="form-group social-info">
							<div class="col-lg-4">
								<div class="row">
									<label class="col-sm-4 control-label" for="input-social_name-<?php echo $i; ?>"><?php echo $entry_social_name; ?></label>
									<div class="col-sm-8">
										<input type="text" name="socials[<?php echo $i; ?>][name]" value="<?php echo isset($social['name']) ? $social['name'] : ''; ?>" placeholder="<?php echo $entry_social_name; ?>" id="input-social_name-<?php echo $i; ?>" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="row">
									<label class="col-sm-4 control-label" for="input-social_link-<?php echo $i; ?>"><?php echo $entry_social_link; ?></label>
									<div class="col-sm-8">
										<input type="text" name="socials[<?php echo $i; ?>][link]" value="<?php echo isset($social['link']) ? $social['link'] : ''; ?>" placeholder="<?php echo $entry_social_link; ?>" id="input-social_link-<?php echo $i; ?>" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="row">
									<label class="col-sm-4 control-label" for="input-social_css-<?php echo $i; ?>"><?php echo $entry_social_css; ?></label>
									<div class="col-sm-8">
										<input type="text" name="socials[<?php echo $i; ?>][css]" value="<?php echo isset($social['css']) ? $social['css'] : ''; ?>" placeholder="<?php echo $entry_social_css; ?>" id="input-social_css-<?php echo $i; ?>" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="col-lg-1 text-right">
								<button type="button" onclick="removeSocial($(this));" data-toggle="tooltip" title="<?php echo $button_remove_social; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
							</div>
						</div>
						<?php $i++; } ?>
						<?php } ?>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12 text-right">
							<button type="button" onclick="addSocial();" data-toggle="tooltip" title="<?php echo $button_add_social; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function addSocial() {
		var socialCount = $('.social-info').length,
		html = '<div class="form-group social-info">';
		html += '	<div class="col-lg-4">';
		html += '		<div class="row">';
		html += '			<label class="col-sm-4 control-label" for="input-social_name-' + socialCount + '"><?php echo $entry_social_name; ?></label>';
		html += '			<div class="col-sm-8">';
		html += '				<input type="text" name="socials[' + socialCount + '][name]" value="" placeholder="<?php echo $entry_social_name; ?>" id="input-social_name-' + socialCount + '" class="form-control"/>';
		html += '			</div>';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="col-lg-4">';
		html += '		<div class="row">';
		html += '			<label class="col-sm-4 control-label" for="input-social_link-' + socialCount + '"><?php echo $entry_social_link; ?></label>';
		html += '			<div class="col-sm-8">';
		html += '				<input type="text" name="socials[' + socialCount + '][link]" value="" placeholder="<?php echo $entry_social_link; ?>" id="input-social_link-' + socialCount + '" class="form-control"/>';
		html += '			</div>';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="col-lg-3">';
		html += '		<div class="row">';
		html += '			<label class="col-sm-4 control-label" for="input-social_css-' + socialCount + '"><?php echo $entry_social_css; ?></label>';
		html += '			<div class="col-sm-8">';
		html += '				<input type="text" name="socials[' + socialCount + '][css]" value="" placeholder="<?php echo $entry_social_css; ?>" id="input-social_css-' + socialCount + '" class="form-control"/>';
		html += '			</div>';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="col-lg-1 text-right">';
		html += '		<button type="button" onclick="removeSocial($(this));" data-toggle="tooltip" title="<?php echo $button_remove_social; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
		html += '	</div>';
		html += '</div>';
		$('#socials').append(html);
	}
	function removeSocial(elem) {
		elem.closest('.social-info').remove();
	}
</script>
<?php echo $footer; ?>